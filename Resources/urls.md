# Important and useful URLs

## Install Git for Windows
https://gitlab.com/ayossef/dso-full/

## Install WSL (Windows Subsystem for Linux)
- Open a CMD as an Administrator 
```bash
wsl --install
```
- restart the machine

## Downland and install docker desktop for Windows
https://docs.docker.com/desktop/install/windows-install/

## Defect Dojo Git Repo
https://github.com/DefectDojo/django-DefectDojo
